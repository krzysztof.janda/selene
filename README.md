# Selene PDF Converter

[![Tabula][tabula-shield]][tabula-url]

The application converts safety stock reports in PDF to CSV format.

## Built with

* [Maven](https://maven.apache.org) - Dependency Management
* [Gradle](https://gradle.org) - Build automation system
* [Tabula]() - Library for extracting tables from PDF files

## Authors

* [Eternal Apps](http://www.eternalapps.pl) 

<!-- Urls -->
[tabula-shield]: https://img.shields.io/badge/Tabula-1.0.2-blue.svg
[tabula-url]: https://github.com/tabulapdf/tabula-java
