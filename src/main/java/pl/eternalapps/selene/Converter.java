/*
 * Copyright (c) 2019 Eternal Apps
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pl.eternalapps.selene;

import java.io.*;
import java.util.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import technology.tabula.*;
import technology.tabula.extractors.BasicExtractionAlgorithm;
import technology.tabula.writers.Writer;

import javax.swing.*;

class Converter {
    private Configuration configuration;
    private JFrame frame;
    private Rectangle pageArea;

    Converter(JFrame frame) {
        this.frame = frame;

        try {
            this.configuration = Configuration.getInstance();

            Map<String, Float> pageArea = this.configuration.getPageArea();
            this.pageArea = new Rectangle(pageArea.get("top"), pageArea.get("left"), pageArea.get("width"), pageArea.get("height"));
        } catch (Exception ex) {
            ApplicationDialog.error(this.frame, "Wystąpił błąd podczas otwierania pliku konfiguracyjnego.", "SE12.2");
        }
    }

    void extractTablesInto(File pdfFile, File outputFile) {
        BufferedWriter bufferedWriter = null;

        try {
            FileWriter fileWriter = new FileWriter(outputFile.getAbsoluteFile());
            bufferedWriter = new BufferedWriter(fileWriter);

            this.extractFile(pdfFile, bufferedWriter);
        } catch (IOException ex) {
            ApplicationDialog.error(this.frame, "Wystąpił błąd: " + ex.getMessage(), "SE02");
        } finally {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ex) {
                    ApplicationDialog.error(this.frame, "Wystąpił błąd: " + ex.getMessage(), "SE03");
                }
            }
        }
    }

    private void extractFile(File pdfFile, Appendable output) {
        PDDocument pdfDocument = null;

        try {
            pdfDocument = PDDocument.load(pdfFile);
            PageIterator pageIterator = this.getPageIterator(pdfDocument);
            List<Table> tables = new ArrayList<>();

            while (pageIterator.hasNext()) {
                Page page = pageIterator.next();

                tables.addAll(this.extractTables(page.getArea(this.pageArea)));
            }

            this.applyTableFilter(tables);
            this.writeTables(tables, output);
        } catch (IOException ex) {
            ApplicationDialog.error(this.frame, "Wystąpił błąd podczas tworzenia pliku.", "SE04");
        } finally {
            try {
                if (pdfDocument != null) {
                    pdfDocument.close();
                }
            } catch (IOException ex) {
                ApplicationDialog.error(this.frame, "Wystąpił błąd zamykania dokumentu.", "SE05");
            }
        }
    }

    private PageIterator getPageIterator(PDDocument pdfDocument) {
        return new ObjectExtractor(pdfDocument).extract(this.getPagesList(pdfDocument));
    }

    private void writeTables(List<Table> tables, Appendable out) throws IOException {
        Writer writer = new CSVCustomWriter();
        writer.write(out, tables);
    }

    private List<Integer> getPagesList(PDDocument pdfDocument) {
        List<Integer> pages = new ArrayList<>();

        for (int i = this.configuration.getFirstPage(); i <= pdfDocument.getNumberOfPages(); i++) {
            pages.add(i);
        }

        return pages;
    }

    private List<Table> extractTables(Page page) {
        return new BasicExtractionAlgorithm().extract(page, this.configuration.getColumnPositions());
    }

    private void applyTableFilter(List<Table> tables) {
        int index;

        for (Table table : tables) {
            table.getRows().removeIf(cells -> cells.get(0).getText().length() < 3);

            if (this.configuration.doesNormalizeNegativeNumbers()) {
                for (List<RectangularTextContainer> cells : table.getRows()) {
                    index = -1;

                    for (RectangularTextContainer cell: cells) {
                        String cellText = cell.getText();
                        ++index;

                        if (cellText.endsWith("-")) {
                            try {
                                String numberCandidate = cellText.substring(0, cellText.length() - 1);
                                Float.parseFloat(numberCandidate);
                                cellText = "-" + numberCandidate;
                            } catch (NumberFormatException ex) {
                                continue;
                            }

                            cells.set(index, new TextChunk(new TextElement(0, 0, 1, 1, null, 1, cellText, 1)));
                        }
                    }
                }
            }
        }

        tables.get(0).getRows().add(0, this.generateHeaderColumns());
    }

    private ArrayList<RectangularTextContainer> generateHeaderColumns() {
        ArrayList<RectangularTextContainer> headerColumns = new ArrayList<>();

        for (String columnName : this.configuration.getColumnNames()) {
            headerColumns.add(new TextChunk(new TextElement(0, 0, 1, 1, null, 1, columnName, 1)));
        }

        return headerColumns;
    }
}
