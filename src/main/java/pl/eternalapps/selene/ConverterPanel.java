/*
 * Copyright (c) 2019 Eternal Apps
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pl.eternalapps.selene;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class ConverterPanel extends JPanel implements ActionListener {
    private JFileChooser fc;
    private FileFilter openFilter;
    private FileFilter saveFilter;
    private JButton openButton;
    private JFrame frame;
    private Configuration configuration;

    ConverterPanel(JFrame frame) {
        super(new BorderLayout());
        this.frame = frame;

        try {
            this.configuration = Configuration.getInstance();
        } catch (Exception ex) {
            ApplicationDialog.error(this.frame, "Wystąpił błąd podczas otwierania pliku konfiguracyjnego.", "SE12.1");
        }

        openFilter = new FileNameExtensionFilter("Pliki PDF", "pdf");
        saveFilter = new FileNameExtensionFilter("Plik CSV", "csv");

        this.initializeLayout();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == openButton) {
            fc.resetChoosableFileFilters();
            fc.setFileFilter(this.openFilter);

            int returnVal = fc.showOpenDialog(ConverterPanel.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File sourceFile = fc.getSelectedFile();

                fc.resetChoosableFileFilters();
                fc.setFileFilter(this.saveFilter);
                fc.setSelectedFile(new File(sourceFile.getName().replace(".pdf", ".csv")));
                returnVal = fc.showSaveDialog(ConverterPanel.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File targetFile = fc.getSelectedFile();

                    Converter converter = new Converter(frame);
                    converter.extractTablesInto(sourceFile, targetFile);

                    try {
                        if (this.configuration.doesOpenAfterConversion()) {
                            Desktop.getDesktop().open(targetFile);
                        } else {
                            ApplicationDialog.success(this.frame, "Zakończono pomyślnie!");
                        }
                    } catch (IOException ex) {
                        ApplicationDialog.error(this.frame, "Wystąpił błąd podczas otwierania pliku: " + ex.getMessage(), "SE11");
                    }
                }

            }
        }
    }

    private void initializeLayout() {
        fc = new JFileChooser();

        openButton = new JButton("Wybierz plik");
        openButton.addActionListener(this);

        this.add(openButton);
    }
}
