/*
 * Copyright (c) 2019 Eternal Apps
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pl.eternalapps.selene;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Configuration {
    private static final String DEFAULT_PROPERTIES_FILE = "/default.properties";
    private static final String USER_PROPERTIES_FILE = "selene.properties";

    private static Configuration instance;
    private Properties properties;

    private Configuration() throws IOException {
        Properties defaultProperties = new Properties();
        defaultProperties.load(new InputStreamReader(Configuration.class.getResourceAsStream(DEFAULT_PROPERTIES_FILE), StandardCharsets.UTF_8));

        try {
            this.properties = new Properties(defaultProperties);
            this.properties.load(new InputStreamReader(new FileInputStream(USER_PROPERTIES_FILE), StandardCharsets.UTF_8));
        } catch (IOException ex) {
            // No configuration file. Don't worry, we should have default values.
        }
    }

    static Configuration getInstance() throws IOException {
        if (instance == null) {
            instance = new Configuration();
        }

        return instance;
    }

    boolean doesOpenAfterConversion() {
        return Boolean.parseBoolean(this.properties.getProperty("app.openAfterConversion"));
    }

    int getFirstPage() {
        return Integer.parseInt(this.properties.getProperty("app.firstPage"));
    }

    String[] getColumnNames() {
        return this.properties.getProperty("column.names").split("\\|");
    }

    List<Float> getColumnPositions() {
        return Arrays.stream(this.properties.getProperty("column.positions").split("\\|"))
                .map(Float::parseFloat)
                .collect(Collectors.toList());
    }

    Map<String, Float> getPageArea() {
        return Stream.of(new Object[][]{
                {"top", Float.parseFloat(this.properties.getProperty("pageArea.top"))},
                {"left", Float.parseFloat(this.properties.getProperty("pageArea.left"))},
                {"width", Float.parseFloat(this.properties.getProperty("pageArea.width"))},
                {"height", Float.parseFloat(this.properties.getProperty("pageArea.height"))},
        }).collect(Collectors.collectingAndThen(
                Collectors.toMap(data -> (String) data[0], data -> (float) data[1]),
                Collections::unmodifiableMap));
    }

    boolean doesNormalizeNegativeNumbers() {
        return Boolean.parseBoolean(this.properties.getProperty("app.normalizeNegativeNumbers"));
    }
}
